#!/bin/bash

# Bash "strict mode"
set -euo pipefail

# Tell apt that no manual input will be given
export DEBIAN_FRONTEND=noninteractive

apt update
apt install -y ca-certificates jq
apt install -y --no-install-recommends apt-utils
wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | apt-key add -
echo "deb https://packages.cloudfoundry.org/debian stable main" | tee /etc/apt/sources.list.d/cloudfoundry-cli.list
apt-get update
apt-get install cf7-cli
cf install-plugin -f multiapps

# Cleanup
apt-get clean

rm -rf /var/lib/apt/lists/*
