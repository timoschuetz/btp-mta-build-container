FROM ghcr.io/sap/mbtci-java11-node14:latest

COPY install-packages.sh .
RUN ["chmod", "+x", "./install-packages.sh"]
RUN ./install-packages.sh
